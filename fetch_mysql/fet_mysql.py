from flask import Flask
from flask_mysqldb import MySQL
import json
import time

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'aero'
app.config['MYSQL_DB'] = 'Eziosense'

# Intialize MySQL
mysql = MySQL(app)


@app.route('/')
def users():
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM Ezio order by datetime desc limit 10''')
    rv = cur.fetchall()
    for row in rv:
        date = row[1]
        device_id = row[2]
        pm25 = row[5]
        temp = row[8]
        hum = row[9]
        data = {"Date" : date,
        		"Device" : device_id,
        		"PM 2.5" : pm25,
        		"Temperature" : temp,
        		"Humidity" : hum}
        data_json = json.dumps(data) 
    return data_json       		

if __name__ == '__main__':
    app.run(debug=True)